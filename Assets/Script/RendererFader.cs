﻿using UnityEngine;
using System.Collections;

public class RendererFader : MonoBehaviour
{
	public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
	
	
	private bool sceneStarting = false;      // Whether or not the scene is still fading in.

	private bool sceneEnding = false;
	
	
	void Awake ()
	{
		// Set the texture so that it is the the size of the screen and covers it.
		//renderer.material.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
		float alpha = 0f;
		Color color = renderer.material.color;
		color = new Color(color.r, color.g, color.b, alpha);
		renderer.material.color = color;
	}
	
	
	void Update ()
	{
		// If the scene is starting...
		if(sceneStarting)
			StartScene();
		else if (sceneEnding){
			EndScene();
		}
		//Debug.Log ("alpha: " + renderer.material.color.a);
	}
	
	
	void FadeToClear ()
	{
		// Lerp the colour of the texture between itself and transparent.
		// Lerp the colour of the texture between itself and black.
		float alpha;
		Color color = renderer.material.color;
		alpha = Mathf.Lerp(renderer.material.color.a, 1f, fadeSpeed * Time.deltaTime);
		color = new Color(color.r, color.g, color.b, alpha);
		renderer.material.color = color;
		//renderer.material.color = Color.Lerp(renderer.material.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	
	
	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		float alpha;
		Color color = renderer.material.color;
		alpha = Mathf.Lerp(renderer.material.color.a, 0f, fadeSpeed * Time.deltaTime);
		color = new Color(color.r, color.g, color.b, alpha);
		renderer.material.color = color;
	}
	
	
	void StartScene ()
	{
		// Fade the texture to clear.
		FadeToClear();
		
		// If the texture is almost clear...
		if(renderer.material.color.a >= 0.95f)
		{
			// ... set the colour to clear and disable the GUITexture.
			Color color = renderer.material.color;
			renderer.material.color = new Color (color.r, color.g, color.b, 1f);
			
			// The scene is no longer starting.
			sceneStarting = false;
		}
	}
	
	
	public void EndScene ()
	{
		// Make sure the texture is enabled.
		renderer.enabled = true;
		
		// Start fading towards black.
		FadeToBlack();
		
		// If the screen is almost black...
		if(renderer.material.color.a <= 0.05f){
			Color color = renderer.material.color;
			renderer.material.color = new Color (color.r, color.g, color.b, 0f);
			sceneEnding = false;
			renderer.enabled = false;
		}
	}

	public void SetSceneStarting(bool _sceneStarting){
		sceneStarting = _sceneStarting;
	}

	public void SetSceneEnding(bool _sceneEnding){
		sceneEnding = _sceneEnding;
	}


}
