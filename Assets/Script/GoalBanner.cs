﻿using UnityEngine;
using System.Collections;

public class GoalBanner : MonoBehaviour {
	bool isGoal;
	float minYScale = 0f;
	float maxYScale = 1f;
	float scale;
	float goalStartTime;
	float TIME_DELAY = DataStorage.MINIMUM_DELAY_BETWEEN_TWO_ATTEMPTS;
	float scalingTimeFraction = 0.2f; // the fraction o TIME_DELAY it takes to scale


	// Use this for initialization
	void Start () {
		isGoal = false;
		gameObject.transform.localScale = new Vector3(1f,0f,1f);
	}
	
	// Update is called once per frame
	void Update () {
		if (isGoal && scale < 1f){
			scale = Mathf.Clamp ((Time.time - goalStartTime)/ (scalingTimeFraction * TIME_DELAY), 
		    	                 minYScale, maxYScale);
		} else if (!isGoal && scale > 0f){
			scale = Mathf.Clamp (1f - (Time.time - goalStartTime)/ (scalingTimeFraction * TIME_DELAY), 
			                     minYScale, maxYScale);
		}
		gameObject.transform.localScale = new Vector3(1f,scale,1f);
	}

	void Goal(){
		isGoal = true;
		goalStartTime = Time.time;
		Invoke("RemoveBanner", TIME_DELAY);
	}

	void RemoveBanner(){
		isGoal = false;
		goalStartTime = Time.time;
	}

}
