﻿using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour {
	int scoreA;
	int scoreB;
	int remainingAttempsA;
	int remainingAttempsB;
	bool isTurnA;
	bool startedByA;
	bool gameEnded;
	bool extraBall;
	bool endGameAnimation;
	bool playerAWins;
	public GameObject prefabGoal;
	public GameObject prefabMissed;
	public GameObject prefabGoalExtra;
	public GameObject prefabMissedExtra;
	GameObject[] attemptsA;
	GameObject[] attemptsB;
	float TIME_DELAY = DataStorage.MINIMUM_DELAY_BETWEEN_TWO_ATTEMPTS;
	float SCORE_DELAY = DataStorage.TIME_SHOWING_FINAL_SCORE;
	bool newBall;


	// Use this for initialization
	void Start () {
		attemptsA = GameObject.FindGameObjectsWithTag("AttemptsA");
		attemptsB = GameObject.FindGameObjectsWithTag("AttemptsB");

		StartANewGame();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool GetIsTurnA(){
		return isTurnA;
	}

	void SetNewBallTrue(){
		newBall = true;
	}

	public void Goal(){
		if (newBall){
			newBall = false;
			//Debug.Log ("GC-Goal");
			if (isTurnA){
				scoreA++;
				AGoalCheckmark();
			} else {
				scoreB++;
				BGoalCheckmark();
			}
			Invoke("UpdateData", TIME_DELAY);
		}
	}

	void AGoalCheckmark(){
		//Debug.Log ("GC-AGoalCheckmark");
		int attemptNumber = 6-remainingAttempsA; // the names of the ball GameObjects contains a number 1-5
		if (GameObject.FindGameObjectsWithTag("Checkmarks").Length < 10){
			foreach (GameObject attempt in attemptsA){
				if (attempt.name.Contains(attemptNumber.ToString()))
				{
					Instantiate(prefabGoal, attempt.transform.position, Quaternion.identity);
				}
			}
		}
		else {
			foreach (GameObject attempt in attemptsA){
				if (attempt.name.Contains("Extra"))
				{
					Instantiate(prefabGoalExtra, attempt.transform.position, Quaternion.identity);
				}
			}
		}
	}

	void BGoalCheckmark(){
		//Debug.Log ("GC-BGoalCheckmark");
		int attemptNumber = 6-remainingAttempsB; // the names of the ball GameObjects contains a number 1-5
		if (GameObject.FindGameObjectsWithTag("Checkmarks").Length < 10){
			foreach (GameObject attempt in attemptsB){
				if (attempt.name.Contains(attemptNumber.ToString()))
				{
					Instantiate(prefabGoal, attempt.transform.position, Quaternion.identity);
				}
			}
		}
		else {
			foreach (GameObject attempt in attemptsB){
				if (attempt.name.Contains("Extra"))
				{
					Instantiate(prefabGoalExtra, attempt.transform.position, Quaternion.identity);
				}
			}
		}
	}
	
	public void Miss(){
		if (newBall){
			newBall = false;
			//Debug.Log ("GC-Miss");
			if (isTurnA){
				AMissCheckmark();
			} else {
				BMissCheckmark();
			}
			Invoke("UpdateData", TIME_DELAY);
		}
	}

	void AMissCheckmark(){
		//Debug.Log ("GC-AMissCheckmark");
		int attemptNumber = 6-remainingAttempsA; // the names of the ball GameObjects contains a number 1-5
		if (GameObject.FindGameObjectsWithTag("Checkmarks").Length < 10){
			foreach (GameObject attempt in attemptsA){
				if (attempt.name.Contains(attemptNumber.ToString()))
				{
					Instantiate(prefabMissed, attempt.transform.position, Quaternion.identity);
				}
			} 
		}
		else {
			foreach (GameObject attempt in attemptsA){
				if (attempt.name.Contains("Extra"))
				{
					Instantiate(prefabMissedExtra, attempt.transform.position, Quaternion.identity);
				}
			}
		}
	}
	
	void BMissCheckmark(){
		//Debug.Log ("GC-BMissCheckmark");
		int attemptNumber = 6-remainingAttempsB; // the names of the ball GameObjects contains a number 1-5
		if (GameObject.FindGameObjectsWithTag("Checkmarks").Length < 10){
			foreach (GameObject attempt in attemptsB){
				if (attempt.name.Contains(attemptNumber.ToString()))
				{
					Instantiate(prefabMissed, attempt.transform.position, Quaternion.identity);
				}
			} 
		}
		else {
			foreach (GameObject attempt in attemptsB){
				if (attempt.name.Contains("Extra"))
				{
					Instantiate(prefabMissedExtra, attempt.transform.position, Quaternion.identity);
				}
			}
		}
	}

void UpdateData(){
		//Debug.Log ("GC-UpdateData");
		AddKick();
		int[] score = {scoreA, scoreB};
		BroadcastMessage("UpdateScore", score, SendMessageOptions.DontRequireReceiver);
		TestWinState();
		if (!gameEnded){
			isTurnA = !isTurnA;
			// TODO: check if IsTurnA is running before SetArrowPosition
			BroadcastMessage("IsTurnA", isTurnA, SendMessageOptions.DontRequireReceiver);
			bool[] booleans = { extraBall, isTurnA, isTurnA == startedByA };
			//if (isTurnA == startedByA)
			BroadcastMessage("SetArrowPosition", booleans, SendMessageOptions.DontRequireReceiver);
			BroadcastMessage("ChangeTextToNextPlayer", !isTurnA, SendMessageOptions.DontRequireReceiver);
		}
	}

	///	Update remainingAttempsA\B and player's name on GUI
	void AddKick(){
		//Debug.Log ("GC-AddKick");
		if (isTurnA) {
			remainingAttempsA--;
			//script_PlayerAGUIBehaviour.ChangeTextToNextPlayer();
		}
		else {
			remainingAttempsB--;
			//script_PlayerBGUIBehaviour.ChangeTextToNextPlayer();
		}
	}
 
	void TestWinState(){
		//Debug.Log ("GC-TestWinState");
		if (scoreA == scoreB && remainingAttempsA == 0 && remainingAttempsB == 0){
			remainingAttempsA++;
			remainingAttempsB++;
			foreach (GameObject checkmarkExtra in GameObject.FindGameObjectsWithTag("CheckmarksExtra")){
				Destroy(checkmarkExtra);
			}
			extraBall = true;
			BroadcastMessage("GetExtraBall", SendMessageOptions.DontRequireReceiver);
		}
		if (remainingAttempsA < (scoreB - scoreA)){
			gameEnded = true;
			//Debug.Log("Player B wins");
			playerAWins = false;
			//script_PlayerAGUIBehaviour.ResetToFirstPlayer();  
			//script_PlayerBGUIBehaviour.ResetToFirstPlayer();
		} else if (remainingAttempsB < (scoreA - scoreB)){
			gameEnded = true;
			//Debug.Log("Player A wins");
			playerAWins = true;
			//script_PlayerAGUIBehaviour.ResetToFirstPlayer();
			//script_PlayerBGUIBehaviour.ResetToFirstPlayer();
		}
	}

	void GameEnded(){
		//Debug.Log ("GC-GameEnded");
		if (!endGameAnimation){
			BroadcastMessage("DisplayPlacar", playerAWins, SendMessageOptions.DontRequireReceiver);
			Invoke ("StartANewGame", SCORE_DELAY);
			endGameAnimation = true;
		}
	}

	void StartANewGame(){
		//Debug.Log ("GC-StartANewGame");
		scoreA = 0;
		scoreB = 0;
		newBall = true;
		remainingAttempsA = 5;
		remainingAttempsB = 5;
		isTurnA = true; //(Random.value >= 0.5f);
		startedByA = isTurnA;
		gameEnded = false;
		extraBall = false;
		endGameAnimation = false;
		BroadcastMessage("ResetExtraBall", SendMessageOptions.DontRequireReceiver);
		BroadcastMessage("ResetArrowPosition", extraBall, SendMessageOptions.DontRequireReceiver);
		BroadcastMessage("IsTurnA", isTurnA, SendMessageOptions.DontRequireReceiver);
		BroadcastMessage("ResetToFirstPlayer", SendMessageOptions.DontRequireReceiver);
		BroadcastMessage("SetArrowPosition", new bool[] {false, isTurnA, false}, SendMessageOptions.DontRequireReceiver); //extraball, isTurnA, stepForward
		int[] score = {scoreA, scoreB};
		BroadcastMessage("UpdateScore", score, SendMessageOptions.DontRequireReceiver);
		foreach (GameObject checkmarkExtra in GameObject.FindGameObjectsWithTag("CheckmarksExtra")){
			Destroy(checkmarkExtra);
		}
		foreach (GameObject checkmark in GameObject.FindGameObjectsWithTag("Checkmarks")){
			Destroy(checkmark);
		}
	}

	public bool GetEndGame(){
		return gameEnded;
	}

	void OnGUI(){
		if (GUI.Button(new Rect(5, Screen.height - 50, 40, 40), "Sair")){
			BroadcastMessage("SetEndGame", true, SendMessageOptions.DontRequireReceiver);
		}
	}

}
