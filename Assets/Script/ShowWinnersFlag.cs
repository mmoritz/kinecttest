﻿using UnityEngine;
using System.Collections;

public class ShowWinnersFlag : MonoBehaviour {
	public bool isA;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DisplayPlacar(bool _playerAWins){
		if (_playerAWins)
			gameObject.renderer.enabled = isA;
		else
			gameObject.renderer.enabled = !isA;
	}
}
