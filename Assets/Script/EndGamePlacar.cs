﻿using UnityEngine;
using System.Collections;

public class EndGamePlacar : MonoBehaviour {
	//Vector3 startPosition;
	bool enter;
	float startX = -1f;
	float endX = 0.5f;
	float currentX;
	float displayStartTime;
	float SCORE_DELAY = DataStorage.TIME_SHOWING_FINAL_SCORE;
	float enteringTimeFraction = 0.2f; // the fraction o TIME_DELAY it takes to scale

	// Use this for initialization
	void Start () {
		enter = false;
		//startPosition = new Vector3(-1f, 0.45f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (enter && currentX < endX){
			currentX = Mathf.Clamp ((Time.time - displayStartTime)/ (enteringTimeFraction * SCORE_DELAY), 
			                        startX, endX);
		} else if (!enter && currentX > startX){
			currentX = Mathf.Clamp (endX - (Time.time - displayStartTime)/ (enteringTimeFraction * SCORE_DELAY), 
			                     startX, endX);
		}
		gameObject.transform.position = new Vector3(currentX,0.45f, 0);
	}

	void DisplayPlacar(){
		displayStartTime = Time.time;
		enter = true;
		Invoke("RemoveBanner", SCORE_DELAY);
	}

	void RemoveBanner(){
		displayStartTime = Time.time;
		enter = false;
	}
}
