﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimatorController : MonoBehaviour {

	protected Animator animator;
	Collider2D[] listOfColliders;
	GameObject ball;
	BallControl scriptBallControl;
	float thresholdRightLeft = 0.9f;
	float thresholdUp = 1.4f;
	float thresholdMiddle = 0f;
	float ballZPositionToStartJumping = 10f;
	float ballZPositionToGuessJumping = 18f;
	bool brasil, argentina;
	GameObject gameController;
	int mJump;
	int guessJump;
	Texture2D mTexture;

	string[] sprites = new string[34];

	// Use this for initialization
	void Start () {
		ball = GameObject.FindGameObjectWithTag ("Bola") as GameObject;
		scriptBallControl = ball.GetComponent<BallControl>();

		for (int i=0; i<17; i++) {
			sprites[i] = "spritesheet_goleiro_penalti_"+i;
		}

		for (int i=17; i<34; i++) {
			sprites[i] = "spritesheet_goleiro_penalti_left_"+(i-17);
		}

		animator = GetComponent<Animator> ();
		listOfColliders = gameObject.GetComponents<Collider2D>();
		gameController = GameObject.FindGameObjectWithTag("GameController");
		mJump = 0;
		guessJump = -1;

		SetTexture2D();
	}
	
	// Update is called once per frame
	void Update () {
		if (animator) {
			UpdateCollider();
			//get the current state
			//AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
			animator.SetBool("gameRunning", JumpAllowedFromZ(1000f));

			//if we're in "Run" mode, respond to input for jump, and set the Jump parameter accordingly. 
			if(IsWaiting())
			{
				if (RandomJump()){
					// randomly select between a random jump or wait to decide
					if (guessJump == -1){
						// malandragon! se o brasil chutar 50% de chance do goleiro tentar adivinhar o canto
						// se a argentina chutar, o goleiro sempre espera pra pular na bola!
						if (gameController.GetComponent<GameControl>().GetIsTurnA() && !TwoHumanPlayers())
							guessJump = 1;//Random.Range(0,2);
						else
							guessJump = 1;

					} else {
						if (guessJump > 0) {
							if (JumpAllowedFromZ(ballZPositionToStartJumping))
								SetAutoWhereToJump();
						}
						else {
							if (JumpAllowedFromZ(ballZPositionToGuessJumping)) //jump sooner
								animator.SetInteger("whereToJump", Random.Range(0, 8)); //Random.Range(0, 8)
						}
					}
				} else {
					// set mJump using kinect input or mouse input
					if(InputFromKinect ()){
						if (GetKinectInput() != -1){
							mJump = GetKinectInput ();
						}
					} else {
						if (GetMouseInput() != -1){
							mJump = GetMouseInput();
						}
					}

					// set animator "whereToJump" using mJump
					// FIXME: loopando aqui, nao deveria estar
					if(JumpAllowedFromZ(ballZPositionToGuessJumping)){
						animator.SetInteger("whereToJump", mJump);
					}
					if (mJump == 0 && scriptBallControl.GetGoal()){
						if (ball.transform.position.x > 0f)
							animator.SetInteger ("whereToJump", 8); // owned-right
						else
							animator.SetInteger ("whereToJump", 9); // owned-left
					} else if(mJump == 0 && scriptBallControl.GetMissed()){
						animator.SetBool("greatSafe", true);
					}
				}
			}
			else
			{
				// restart data
				animator.SetInteger("whereToJump", 0);
				animator.SetBool("greatSafe", false);
				mJump = 0;
				guessJump = -1;
			}
		}
	}

	// returns true if the goal keeper jump will be controlled by a the computer, false otherwise
	bool RandomJump(){
		return ((!argentina && gameController.GetComponent<GameControl>().GetIsTurnA()) ||
			(!brasil && !gameController.GetComponent<GameControl>().GetIsTurnA()));
	}

	bool TwoHumanPlayers(){
		return (scriptBallControl.GetBrasil() && scriptBallControl.GetArgentina());
	}

	// permit the goal keeper to jump if the ball is nearer than z, from the goal.
	bool JumpAllowedFromZ(float _z){
		//return (scriptBallControl.GetKickedOnce() && ball.transform.position.z > -_z);
		return (scriptBallControl.CompareWithCurrentState(BallControl.BallState.Flying) ||
		        scriptBallControl.CompareWithCurrentState(BallControl.BallState.Resolved) ||
		        scriptBallControl.CompareWithCurrentState(BallControl.BallState.Reseting) && 
		        ball.transform.position.z > -_z);
	}

	void SetAutoWhereToJump(){
		if (ball.transform.position.x > thresholdRightLeft){ // right
			if (ball.transform.position.y > thresholdUp)
				animator.SetInteger("whereToJump", 3); // upper-right
			else if(ball.transform.position.y > thresholdMiddle)
				animator.SetInteger("whereToJump", 2); // mid-right
			else
				animator.SetInteger("whereToJump", 1); // bottom-right
		}
		else if (ball.transform.position.x < -thresholdRightLeft){
			if (ball.transform.position.y > thresholdUp)
				animator.SetInteger("whereToJump", 6); // upper-left
			else if(ball.transform.position.y > thresholdMiddle)
				animator.SetInteger("whereToJump", 5); // mid-left
			else
				animator.SetInteger("whereToJump", 4); // bottom-left
		}
		else {
			if (ball.transform.position.y > thresholdUp)
				animator.SetInteger("whereToJump", 7); // jump-up
			else
				animator.SetInteger("whereToJump", 0); // stay
		}
	}

	bool InputFromKinect(){
		if (GameObject.FindGameObjectWithTag("Kinect"))
			return (GameObject.FindGameObjectWithTag("Kinect").name.Equals("KinectPointMan"));
		else return false;
	}

	void UpdateCollider(){
		//Debug.Log("current Sprite: " + GetComponent<SpriteRenderer> ().sprite.name);
		for (int i=0; i<34; i++) {
			if (GetComponent<SpriteRenderer> ().sprite.name.Equals(sprites[i])){
				listOfColliders[i].enabled = true;
			}
			else{
				listOfColliders[i].enabled = false;
			}
		}
	}

	public void GreatSafe(){
		animator.SetBool("greatSafe", true);
	}

	void SetTexture2D(){
		mTexture = new Texture2D(1,1);
		
		for (int y=0;y<mTexture.height;y++){
			for(int x=0;x<mTexture.width;x++){
				mTexture.SetPixel(x,y,Color.red);
			}
		}
		mTexture.Apply();
	}

	void OnGUI(){
		if (DataStorage.DEBUG){
			int labelStartPosX, labelStartPosY, labelSizeX, labelSizeY;
			int sliderStartPosX, sliderStartPosY, sliderSizeX, sliderSizeY;
			//int stepX;
			int stepY;
			labelStartPosX = 5;
			labelStartPosY = 65;
			labelSizeX = 100;
			labelSizeY = 20;
			sliderStartPosX = 10;
			sliderStartPosY = 85; 	
			sliderSizeX = 60;
			sliderSizeY = 30;
			//stepX = 115;
			stepY = 50;

			GUI.skin.toggle.alignment = TextAnchor.UpperLeft;
			GUI.skin.horizontalSlider.normal.background = mTexture;
			//GUI.color = Color.white;
			//GUI.backgroundColor = Color.black;

			GUI.Label(new Rect(labelStartPosX, labelStartPosY, labelSizeX,labelSizeY), 
			          "Lim X: " + thresholdRightLeft.ToString("f1"));
			//GUI.Label(new Rect(labelStartPosX+stepX, labelStartPosY, labelSizeX,labelSizeY), 
			          //thresholdRightLeft.ToString());
			thresholdRightLeft = GUI.HorizontalSlider(new Rect(sliderStartPosX, sliderStartPosY, sliderSizeX, sliderSizeY), 
			                                          thresholdRightLeft, 0.001F, 1.0F);

			GUI.Label(new Rect(labelStartPosX, labelStartPosY+stepY, labelSizeX,labelSizeY), 
			          "Lim Up " + thresholdUp.ToString("f1"));
			//GUI.Label(new Rect(labelStartPosX+stepX, labelStartPosY+stepY, labelSizeX,labelSizeY), 
			          //thresholdUp.ToString());
			thresholdUp = GUI.HorizontalSlider(new Rect(sliderStartPosX, sliderStartPosY+stepY, sliderSizeX, sliderSizeY), 
			                                   thresholdUp, -5F, 5.0F);


			GUI.Label(new Rect(labelStartPosX, labelStartPosY+2*stepY, labelSizeX,labelSizeY), 
			          "Lim Mid: " + thresholdMiddle.ToString("f1"));
			//GUI.Label(new Rect(labelStartPosX+stepX, labelStartPosY+2*stepY, labelSizeX,labelSizeY), 
			          //thresholdMiddle.ToString());
			thresholdMiddle = GUI.HorizontalSlider(new Rect(sliderStartPosX, sliderStartPosY+2*stepY, sliderSizeX, sliderSizeY), 
			                                       thresholdMiddle, -5F, 3.0F);

			GUI.Label(new Rect(labelStartPosX, labelStartPosY+3*stepY, labelSizeX,labelSizeY), 
			          "Z Jump: " + ((int)ballZPositionToStartJumping).ToString());
			//GUI.Label(new Rect(labelStartPosX+stepX, labelStartPosY+3*stepY, labelSizeX,labelSizeY), 
			          //thresholdMiddle.ToString());
			ballZPositionToStartJumping = GUI.HorizontalSlider(new Rect(sliderStartPosX, sliderStartPosY+3*stepY, sliderSizeX, sliderSizeY), 
			                                                   ballZPositionToStartJumping, 0F, 20F);

			// Goleiros
			GUI.skin.toggle.alignment = TextAnchor.UpperLeft;
			GUI.Label(new Rect(Screen.width - labelStartPosX - 80f, labelStartPosY+4.5f*stepY, labelSizeX,labelSizeY), 
			          "Goleiros:");
			brasil = GUI.Toggle(new Rect(Screen.width  - labelStartPosX - 80f, labelStartPosY+5*stepY, labelSizeX, labelSizeY), 
			                    brasil, "Brasil");
			argentina = GUI.Toggle(new Rect(Screen.width  - labelStartPosX - 80f, labelStartPosY+6*stepY, labelSizeX, labelSizeY),  argentina, 
			                       "Argentina");
		}
	}

	public bool IsWaiting(){
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

		return (stateInfo.nameHash == Animator.StringToHash("Base Layer.waiting"));
	}

	int GetMouseInput(){
		Vector3 mousePos;
		if (Input.GetButtonDown("Fire1")){
			mousePos = Input.mousePosition;
			mousePos = new Vector3(mousePos.x/Screen.width, mousePos.y/Screen.height,0f);
			if (mousePos.x > 0.6f){
				if(mousePos.y > 0.77f){
					return 3; // upper-right
				}
				else if(mousePos.y > 0.67f){
					return 2; // mid-right
				}
				else {
					return 1;
				}
			}
			else if(mousePos.x < 0.4f){
				if(mousePos.y > 0.77f){
					return 6; // upper-right
				}
				else if(mousePos.y > 0.67f){
					return 5; // mid-right
				}
				else {
					return 4;
				}
			}
			else {
				if(mousePos.y > 0.77f){
					return 7;
				}
				else {
					return 0;
				}
			}
		} else {
			return -1;
		}
	}

	// retorna 0 a 7 para definir a direçao do salto do goleiro. Retorna -1 se indefinido.
	int GetKinectInput(){
		// TODO: put your code here!
		return -1;
	}
}
