﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallControl : MonoBehaviour
{
	bool kickedOnce = false;
	Vector3 startPosition;
	bool goal = false;
	bool missed = false;
	//float tol = 1f;
	GameObject goleiro;
	AnimatorController scriptAnimatorController;
	float maxScale = 1.0f;
	float minScale = 0.65f;
	GameObject gameController;
	float TIME_DELAY = DataStorage.MINIMUM_DELAY_BETWEEN_TWO_ATTEMPTS;
	float SCORE_DELAY = DataStorage.TIME_SHOWING_FINAL_SCORE;
	bool brasil;
	bool argentina;

	GameObject[] triggers;
	AudioSource audio3;
	AudioSource audio4;

	GameObject[] kinectObjects;
	GameObject mouseFoot;

	float ComputerKickForce = 220f;
	float ComputerKickRangeX = 2.5f;
	float ComputerKickMinY = 2f, ComputerKickMaxY = 4f;
	float ComputerKickZ = 5.5f;

	Texture2D mTexture;


	//ArrayList collidedKickers;	//	up to two GameObjects (we expect a foot, an ankle, or both)
	//bool kicksBlocked;	//	flag blobking or allowing kicks

	public enum BallState 
	{ AtPenaltySpot, Wait, Ready, GettingKickedByComputer, Flying, Resolved, Reseting };
	BallState ballState;
	BallState debugBallState;

	bool loadNextLevel = false;
	
	// Use this for initialization
	void Start ()
	{
		// *********** CARACTERISTICAS DO GAME OBJECT BOLA *******************
		// armazena a posiçao inicial da bola
		startPosition = transform.position;

		// define o tamanho inicial da bola para efeito de perspectiva
		transform.localScale = new Vector3(maxScale, maxScale, maxScale);

		// desabilita a gravidade. Ela sera habilitada somente depois do chute
		rigidbody.useGravity = false;

		// le se a bola tem rastro ou nao, para escolher, editar DataStorage.cs
		GetComponent<TrailRenderer>().enabled = DataStorage.BALL_TRAIL;

		// audio3 = som do chute, audio4 = som de lamentaçao da torcida
		AudioSource[] audioSources = gameObject.GetComponents<AudioSource>() as AudioSource[];
		if (audioSources.Length > 3){
			audio3 = audioSources[2];
			audio4 = audioSources[3];
		}

		// define se brasil e argentina sao humanos ou nao
		brasil = true;
		argentina = false;

		// given brasil == true; and GameControl.isTurnA == true;
		ballState = BallState.AtPenaltySpot;
		// *******************************************************************


		// ***************** OUTROS  GAME OBJECTS ****************************
		// o objeto gameController controla placar, a vez dos jogadores e os banners de gol e placar final
		gameController = GameObject.FindGameObjectWithTag("GameController");

		// o objeto goleiro possui a animaçao do goleiro
		goleiro = GameObject.FindGameObjectWithTag ("Goleiro") as GameObject;
		scriptAnimatorController = goleiro.GetComponent<AnimatorController> ();


		// objetos para detecçao de eventos gol ou fora
		int numTriggers = 
			GameObject.FindGameObjectsWithTag("TriggerFora").Length +
			GameObject.FindGameObjectsWithTag("TriggerGol").Length;

		triggers = new GameObject[numTriggers];
		//Debug.Log("numTriggers" + numTriggers);

		int triggersIndexCounter = 0;
		foreach(GameObject thisObject in GameObject.FindGameObjectsWithTag("TriggerFora")){
			triggers[triggersIndexCounter] = thisObject;
			triggersIndexCounter++;
		}
		foreach(GameObject thisObject in GameObject.FindGameObjectsWithTag("TriggerGol")){
			triggers[triggersIndexCounter] = thisObject;
			triggersIndexCounter++;
		}
		/*if (missed)
			Debug.Log ("");*/

		kinectObjects = GameObject.FindGameObjectsWithTag("Kinect");
		foreach(GameObject gObj in kinectObjects){
			if (gObj.name == "MouseFoot"){
				mouseFoot = gObj;
			}
		}
		KinectObjectsEnabled(false, "Start");

		//collidedKickers = new ArrayList();
		//kicksBlocked    = false;
		SetTexture2D();
	}

	// Update is called once per frame
	void Update ()
	{
		UpdateKickersAvaibility();
		DebugBallState();
		UpdateBallState();
		DebugBallState();
		ScaleTheBall ();
		if (!gameController.GetComponent<GameControl>().GetEndGame()){
			if (IsAComputerTurn() && ballState == BallState.Ready){
				Invoke("KickAuto", TIME_DELAY);
			}
			BallGoingOut();

			if (goal){
				ReduceSpeed();
			}
		} else {
			SleepObjectCollider(TIME_DELAY + SCORE_DELAY);
			gameController.BroadcastMessage ("GameEnded", SendMessageOptions.DontRequireReceiver);
			//Invoke ("ShowScoreAndLoadNextLevel", SCORE_DELAY);
		}
	}

	void ShowScoreAndLoadNextLevel(){
		if (!loadNextLevel){
			loadNextLevel = true;
			gameController.BroadcastMessage ("SetEndScene", true, SendMessageOptions.DontRequireReceiver);
		}
	}

	void GOPhysicsEnabled(GameObject _go, bool _enabled){
		// parent
		SetGOColliderEnabled(_go, _enabled);
		SetGORendererEnabled(_go, _enabled);

		// children
		//List<GameObject> children;
		foreach(Transform child in _go.transform) {
			SetGOColliderEnabled(child.gameObject, _enabled);
			SetGORendererEnabled(child.gameObject, _enabled);
		}

		//children.ForEach(child => child.collider.enabled = _enabled);
		//children.ForEach(child => child.renderer.enabled = _enabled);
	}

	void SetGOColliderEnabled(GameObject _go, bool _enabled){
		if (_go.collider)
			_go.collider.enabled = _enabled;
	}

	void SetGORendererEnabled(GameObject _go, bool _enabled){
		if (_go.renderer)
			_go.renderer.enabled = _enabled;
	}

	void KinectObjectsEnabled(bool _enabled, string _debug=""){
		/*if (_debug != ""){
			Debug.Log (_debug + "->KinectObjectsEnabled");
		}*/
		foreach(GameObject kObj in kinectObjects){
			GOPhysicsEnabled(kObj, _enabled);
		}
	}

	void SetBallStateReady(){
		KinectObjectsEnabled(!IsAComputerTurn(), "SetBallStateReady");
		ballState = BallState.Ready;
	}

	void UpdateBallState(){
		switch ((int)ballState){
		case (int)BallState.AtPenaltySpot:
			gameController.BroadcastMessage("SetNewBallTrue", SendMessageOptions.DontRequireReceiver);
			ballState = BallState.Wait;
			Invoke ("SetBallStateReady", TIME_DELAY);
			break;
		case (int)BallState.Wait:
			break;
		case (int)BallState.Resolved:
			SetTriggers(false);
			KinectObjectsEnabled(false, "Resolved");
			Invoke("SetTriggers", 2f*TIME_DELAY);
			Invoke ("ResetBall",2f*TIME_DELAY);
			if (DataStorage.BALL_TRAIL){
				BallTrail();
				Invoke("BallTrail", 2f*TIME_DELAY);
			}
			ballState = BallState.Reseting;
			break;
		case (int)BallState.GettingKickedByComputer:
			rigidbody.useGravity = true;
			rigidbody.AddForce (
				ComputerKickForce*Random.Range (0.8f, 1f) * //intensidade
				(
				new Vector3 (
				Random.Range (-ComputerKickRangeX, ComputerKickRangeX),	// x
				Random.Range (ComputerKickMinY, ComputerKickMaxY),	// y
				ComputerKickZ)),	// z
				ForceMode.Impulse);
			audio3.Play ();
			ballState = BallState.Flying;
			break;
		case (int)BallState.Ready:
			// FIXME: nao deveria precisar disso
			if (!kinectObjects[0].collider.enabled && !IsAComputerTurn())
				KinectObjectsEnabled(true, "Ready");
			break;
		case (int)BallState.Flying:
			rigidbody.AddForce(100f*Vector3.forward, ForceMode.Force);
			break;
		default:
			break;
		}
	}

	void DebugBallState(){
		if (debugBallState != ballState){
			//Debug.Log ("Ball state: " + ballState);
			debugBallState = ballState;
		}
	}

	void GotKicked(){
		//Debug.Log ("got kicked");
		if (ballState == BallState.Ready){
			audio3.Play ();
			ballState = BallState.Flying;
		}
	}

	bool IsAComputerTurn(){
		return (!argentina &&
		 		!gameController.GetComponent<GameControl>().GetIsTurnA() &&
				 goleiro.GetComponent<AnimatorController>().IsWaiting()) ||
				(!brasil &&
				 gameController.GetComponent<GameControl>().GetIsTurnA() &&
				 goleiro.GetComponent<AnimatorController>().IsWaiting());
	}
	
	void KickAuto(){
		ballState = BallState.GettingKickedByComputer;
		CancelInvoke("KickAuto");
	}

	void SleepObjectCollider(float sleepDuration){
		collider.enabled = false;
		Invoke("AwakeObjectCollider", sleepDuration);
	}

	void AwakeObjectCollider(){
		collider.enabled = true;
	}

	void ScaleTheBall ()
	{
		float scale = Mathf.Clamp (-maxScale*transform.position.z / 20f, minScale, maxScale);
		transform.localScale = new Vector3 (scale, scale, scale);
	}

	void BroadcastGoal(){
		gameController.BroadcastMessage("Goal", SendMessageOptions.DontRequireReceiver);
	}

	void OnTriggerEnter (Collider other)
	{
		ballState = BallState.Resolved;
		if (other.gameObject.tag.Equals ("TriggerGol")  && ballState == BallState.Flying) {
			goal = true;
			Invoke ("BroadcastGoal", TIME_DELAY);
		} else if (other.gameObject.tag.Equals ("TriggerFora") && ballState == BallState.Flying) {
			missed = true;
			gameController.BroadcastMessage("Miss", SendMessageOptions.DontRequireReceiver);
					
			if (scriptAnimatorController) {
				scriptAnimatorController.GreatSafe ();
			}
		} 
	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.gameObject.tag.Equals("Kinect")){		//	experimental foot with mesh collider (capsule)
			rigidbody.useGravity = true;
			kickedOnce = true;
		}
		/*
		else if(collision.gameObject.tag.Equals ("Peh"))	//	may be a foot or ankle of KinectPointMan
		{
			if(!kicksBlocked)
			{
				//Debug.Log("Chute!");
				Rigidbody body = collision.gameObject.rigidbody;
				if (body == null || body.isKinematic)
					return;
				
				bool newCollision = true;
				if(collidedKickers.Count == 1)
				{
					if(collision.gameObject.name == (collidedKickers[0] as GameObject).name)
					{
						Debug.Log("collided with same GameObject. Ignoring this second collision");
						newCollision = false;
					}
				}
				if(newCollision)
				{
					collidedKickers.Add(collision.gameObject);
					Debug.Log ("Kick element collision #" + collidedKickers.Count + " = " + collision.gameObject.name);
					//rigidbody.useGravity = true;
					//kickedOnce = true;

					//Debug.Log("gameObject.transform.position = " + gameObject.transform.position);
					//Debug.Log("other.transform.position = " + collision.gameObject.transform.position);
					//Debug.Log ("F = " + (gameObject.transform.position - collision.transform.position) * 100000);
					//rigidbody.AddForce((gameObject.transform.position - collision.transform.position) * 100000);// *speed
					//rigidbody.AddExplosionForce(power, explosionPos, radius, 3.0F);
					
					//	If we got two contacts (probably ankle and foot), ball is kicked
					if(collidedKickers.Count == 2){
						// APPLAUSEME: Here is my applause! This saved my time in many ways! Thank you Brizoman!
						// Proudly the leader of "Gremio de Compositores do Poçao" & "Coletivo Nos" 
						CancelInvoke("applyKickForce");	//	guarantee that an extra applyKickForce won't be invoked (see else below)
						applyKickForce();
					}
					else {	//	Otherwise, we wait 0.1s and invoke applyKickForce with just one collision
						Invoke("applyKickForce", 0.1f);
					}
				}
			}
		}*/
	}

	/*
	//	[Brizo]
	//	We gather up to 2 collisions to build the kick. This allow us to kick with the lateral part of the foot,
	//which gives more control over the direction
	void applyKickForce()
	{
		rigidbody.useGravity = true;
		kickedOnce = true;
		
		foreach (GameObject kicker in collidedKickers)
		{
			rigidbody.AddForce((gameObject.transform.position - kicker.transform.position) * 400000/collidedKickers.Count);
		}
		Debug.Log(collidedKickers.Count + " elements of collision on kick");
		collidedKickers.Clear();
		
		//	Future kicks get blocked for 1 s
		kicksBlocked = true;
		Invoke ("UnblockKicks", 1);
	}
	
	void UnblockKicks()
	{
		kicksBlocked = false;
	}*/


	void ReduceSpeed(){
		rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, 0.8f*rigidbody.velocity.z);
	}

	void ResetBall(){
		//KinectObjectsEnabled(false);
		transform.position = startPosition;
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
		kickedOnce = false;
		goal = false;
		missed = false;
		rigidbody.useGravity = false;
		ballState = BallState.AtPenaltySpot;
	}
	
	public bool GetKickedOnce ()
	{
		return kickedOnce;
	}

	void SetTriggers(bool _enabled){
		foreach(GameObject thisObject in triggers){
			thisObject.collider.enabled = _enabled;
		}
	}

	void SetTriggers(){
		SetTriggers(true);
	}

	void BallTrail(){
		if (GetComponent<TrailRenderer>().enabled)
			GetComponent<TrailRenderer>().enabled = false;
		else
			GetComponent<TrailRenderer>().enabled = true;
	}

	void BallGoingOut(){
		if (ballState == BallState.Flying){
			bool _missed = false;

			//Debug.Log("z velocity: " + transform.rigidbody.velocity.z);
			if (transform.position.z < -3f && transform.rigidbody.velocity.z < -1f){
				if (!missed && !goal){
					audio4.Play ();
				}
				//Debug.Log("Ball going out, cond1");
				_missed = true;
			}
			else if (Vector3.Distance(transform.position, Vector3.zero) > 25f){
				if (!missed && !goal){
					audio4.Play ();
				}
				//Debug.Log("Ball going out, cond2");
				_missed =  true;
			}
			if(_missed){
				missed = true;
				gameController.BroadcastMessage("Miss", SendMessageOptions.DontRequireReceiver);
				ballState = BallState.Resolved;
			}
		}
	}

	void SetTexture2D(){
		mTexture = new Texture2D(1,1);
		
		for (int y=0;y<mTexture.height;y++){
			for(int x=0;x<mTexture.width;x++){
				mTexture.SetPixel(x,y,Color.red);
			}
		}
		mTexture.Apply();
	}

	void OnGUI(){
		if (DataStorage.DEBUG){
			int labelStartPosX, labelStartPosY, labelSizeX, labelSizeY;
			int sliderStartPosX, sliderStartPosY, sliderSizeX, sliderSizeY;
			//int stepX;
			int stepY;
			labelStartPosX = 5;
			labelStartPosY = 15;
			labelSizeX = 80;
			labelSizeY = 20;
			sliderStartPosX = 10;
			sliderStartPosY = 35;
			sliderSizeX = 60;
			sliderSizeY = 30;
			//stepX = 115;
			stepY = 50;


			// left side
			GUI.Label(new Rect(labelStartPosX, labelStartPosY+5.5f*stepY, labelSizeX,labelSizeY), 
			          "Batedores:");
			GUI.skin.toggle.alignment = TextAnchor.UpperLeft;
			brasil = GUI.Toggle(new Rect(labelStartPosX, labelStartPosY+6*stepY, labelSizeX,labelSizeY), brasil, "Brasil");
			argentina = GUI.Toggle(new Rect(labelStartPosX, labelStartPosY+7*stepY, labelSizeX,labelSizeY), argentina, "Argentina");


			// right side
			GUI.Label(new Rect(Screen.width - labelStartPosX - labelSizeX, labelStartPosY, labelSizeX,labelSizeY), 
			          "ForçaPC: " + ComputerKickForce.ToString("f0"));
			ComputerKickForce = GUI.HorizontalSlider(new Rect(Screen.width - sliderStartPosX - sliderSizeX, sliderStartPosY, sliderSizeX, sliderSizeY), 
			                                         ComputerKickForce, 0f, 500f);

			GUI.Label(new Rect(Screen.width - labelStartPosX - labelSizeX, labelStartPosY+stepY, labelSizeX,labelSizeY), 
			          "RangeX: " + ComputerKickRangeX.ToString("f1"));
			ComputerKickRangeX = GUI.HorizontalSlider(new Rect(Screen.width - sliderStartPosX - sliderSizeX, sliderStartPosY+stepY, sliderSizeX, sliderSizeY), 
			                                          ComputerKickRangeX, 0f, 10f);

			GUI.Label(new Rect(Screen.width - labelStartPosX - labelSizeX, labelStartPosY+2f*stepY, labelSizeX,labelSizeY), 
			          "MinY: " + ComputerKickMinY.ToString("f1"));
			ComputerKickMinY = GUI.HorizontalSlider(new Rect(Screen.width - sliderStartPosX - sliderSizeX, sliderStartPosY+2f*stepY, sliderSizeX, sliderSizeY), 
			                                        ComputerKickMinY, 0f, 10f);


			GUI.Label(new Rect(Screen.width - labelStartPosX - labelSizeX, labelStartPosY+3f*stepY, labelSizeX,labelSizeY), 
			          "MaxY: " + ComputerKickMaxY.ToString("f1"));
			ComputerKickMaxY = GUI.HorizontalSlider(new Rect(Screen.width - sliderStartPosX - sliderSizeX, sliderStartPosY+3f*stepY, sliderSizeX, sliderSizeY), 
			                                        ComputerKickMaxY, 0f, 10f);


			GUI.Label(new Rect(Screen.width - labelStartPosX - labelSizeX, labelStartPosY+4f*stepY, labelSizeX,labelSizeY), 
			          "Z: " + ComputerKickZ.ToString("f1"));
			ComputerKickZ = GUI.HorizontalSlider(new Rect(Screen.width - sliderStartPosX - sliderSizeX, sliderStartPosY+4f*stepY, sliderSizeX, sliderSizeY), 
			                                     ComputerKickZ, 0f, 10f);
		}
	}

	public bool GetMissed(){
		return missed;
	}

	public bool GetGoal(){
		return goal;
	}

	public bool CompareWithCurrentState(BallState _ballState){
		return (ballState == _ballState);
	} 

	void UpdateKickersAvaibility(){
		// se estiver na vez do computador, nao permitir o chute
		if ((!argentina && !gameController.GetComponent<GameControl>().GetIsTurnA()) ||
		    (!brasil && gameController.GetComponent<GameControl>().GetIsTurnA()))
		{
			// TODO: desabilitar o chute com o kinect
			if(mouseFoot)
				mouseFoot.SendMessage("ComputerTurn", true, SendMessageOptions.DontRequireReceiver);
		} else {
			if (mouseFoot)
				mouseFoot.SendMessage("ComputerTurn", false, SendMessageOptions.DontRequireReceiver);
		}
	}

	public bool GetBrasil(){
		return brasil;
	}

	public bool GetArgentina(){
		return argentina;
	}
	
}
