﻿using UnityEngine;
using System.Collections;

public class MoveSprite: MonoBehaviour {
	private bool isMoving;
	//public bool IsMoving{ set {isMoving = value;} get{return isMoving;}}
	[SerializeField]
	private float scrollSpeed;
	[SerializeField]
	private float startPositionVar;
	[SerializeField]
	private float endPositionVar;
	[SerializeField]
	private float scrollDelay;
	[SerializeField]
	private bool vertical;
	[SerializeField]
	private bool negative;
	

	// Use this for initialization
	void Start () {
		if (vertical)
			transform.position = new Vector3(transform.position.x, startPositionVar, transform.position.z);
		else 
			transform.position = new Vector3(startPositionVar, transform.position.y, transform.position.z);
		if (negative)
			scrollSpeed /= -100f;
		else
			scrollSpeed /= 100f;
		Invoke("StartMoving", scrollDelay);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (vertical){
			if (negative) {
				if (isMoving && transform.position.y > endPositionVar){
					transform.position = new Vector3(transform.position.x, transform.position.y+scrollSpeed,transform.position.z);
				} else {
					isMoving = false;
				}
			} else {
				if (isMoving && transform.position.y < endPositionVar){
					transform.position = new Vector3(transform.position.x, transform.position.y+scrollSpeed,transform.position.z);
				} else {
					isMoving = false;
				}
			}

		} else{
			if (negative){
				if (isMoving && transform.position.x > endPositionVar){
					transform.position = new Vector3(transform.position.x+scrollSpeed, transform.position.y, transform.position.z);
				} else {
					isMoving = false;
				}
			} else {
				if (isMoving && transform.position.x < endPositionVar){
					transform.position = new Vector3(transform.position.x+scrollSpeed, transform.position.y, transform.position.z);
				} else {
					isMoving = false;
				}
			}
		}
	}

	public void StartMoving(){
		isMoving = true;
	}

	public bool GetIsMoving(){
		return isMoving;
	}

	public void SetIsMoving(bool _isMoving){
		isMoving = _isMoving;
	}

	public float GetStartPositionVar(){
		return startPositionVar;
	}

	public void SetStartPositionVar(float _startPositionVar){
		startPositionVar = _startPositionVar;
	}

	public float GetEndPositionVar(){
		return endPositionVar;
	}

	public void SetEndPositionVar(float _endPositionVar){
		endPositionVar = _endPositionVar;
	}

	public float GetScrollDelay(){
		return scrollDelay;
	}

	public void SetScrollDelay(float _scrollDelay){
		scrollDelay = _scrollDelay;
	}

	public float GetScrollSpeed(){
		return scrollSpeed;
	}

	public void SetScrollSpeed(float _scrollSpeed){
		scrollSpeed = _scrollSpeed;
	}
}
