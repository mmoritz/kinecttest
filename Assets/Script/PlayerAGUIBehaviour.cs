﻿using UnityEngine;
using System.Collections;

public class PlayerAGUIBehaviour : MonoBehaviour {

	public string [] playersNames = {"7 - Allejo", "1000 - Pele", "7 - Garrincha", "10 - Zico", "11 - Neymar Jr.", "13 - Quequeilson", "12 - Rita Cadillac"};
	int currentPlayerIndex;

	// Use this for initialization
	void Start () {
		currentPlayerIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeTextToNextPlayerA()
	{
		currentPlayerIndex++;
		if(currentPlayerIndex == 7) {
			currentPlayerIndex = 0;
		}
		guiText.text = playersNames[currentPlayerIndex];
	}

	public void ResetToFirstPlayer()
	{
		currentPlayerIndex = 0;
		guiText.text = playersNames[currentPlayerIndex];
	}

	public void alignToParent()
	{
		/*Debug.Log("Parent = " + transform.parent);
		float posX = transform.parent.transform.position.x;
		Debug.Log ("Parent position X = " + posX);
		gameObject.transform.position = new Vector3(posX, transform.position.y, transform.position.z);
		Debug.Log ("current position X = " + gameObject.transform.position);*/
	}
}
