﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlaySound : MonoBehaviour {
	AudioSource audio2;

	// Use this for initialization
	void Start () {
		AudioSource[] audioSources = gameObject.GetComponents<AudioSource>() as AudioSource[];
		if (audioSources.Length > 1)
			audio2 = audioSources[1];
		//Debug.Log("gameObject = " + gameObject);
		//Debug.Log ("audioSources.Length = " + audioSources.Length);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision){
		//Debug.Log("ball collision");
		//	Trave
		if (collision.collider.GetType() == typeof(CapsuleCollider) 
		    && rigidbody.velocity.magnitude > 0.5f 
		    && !collision.gameObject.tag.Equals("Kinect"))
		{
			audio.Play ();
			//Debug.Log ("audio = " + audio);
		}
		else if(collision.collider.GetType() == typeof(BoxCollider) && audio2) {
			audio2.Play();
			//Debug.Log ("audio2 = " + audio2);
		}
	}

	void OnTriggerEnter(Collider collider){
		// FIXME olha a malandragem! 
		// Ainda esta funcionando, mas considere dividir em varios scripts
		if (collider.GetType() == typeof(SphereCollider))
			audio.Play ();
	}

	void OnCollisionEnter2D(Collision2D collision2D){
		if (collision2D.gameObject.rigidbody2D){
			//Debug.Log("ball speed " + collision2D.gameObject.rigidbody2D.velocity.magnitude);
			if (collision2D.collider.GetType() == typeof(CircleCollider2D))
			    //&& collision2D.gameObject.rigidbody2D.velocity.magnitude > .5f)
				audio.Play ();
		}
	}

}
