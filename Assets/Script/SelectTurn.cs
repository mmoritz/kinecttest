﻿using UnityEngine;
using System.Collections;

public class SelectTurn : MonoBehaviour {
	public bool isA;
	//bool tmpIsTurnA;
	Vector3 arrowStartPosition;
	float TIME_DELAY = DataStorage.MINIMUM_DELAY_BETWEEN_TWO_ATTEMPTS;

	// Use this for initialization
	void Start () {
		arrowStartPosition = transform.position;
		//Debug.Log (arrowStartPosition);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// deprecated
	// chamada de GameControl.cs
	public void DelayedIsTurnA(bool _isTurnA){
		Debug.Log ("DelayedIsTurnA");
		//tmpIsTurnA = _isTurnA;
		Invoke("IsTurnANoParam", TIME_DELAY);
	}

	// deprecated
	// sobrecarga que nao requer o parametro: so pode ser usada a partir de DelayedIsTurnA
	void IsTurnANoParam(){
		//Debug.Log("IsTurnANoParam");
		//IsTurnA (tmpIsTurnA);
	}
	

	public void switchA(bool _isTurnA){
		//Debug.Log ("IsTurnA");
		if (_isTurnA)
			gameObject.renderer.enabled = isA;
		else
			gameObject.renderer.enabled = !isA;
	}

	public void SetArrowPosition(bool[] booleans){
		bool _extraBall = booleans[0];
		bool _isTurnA = booleans[1];
		bool _stepForward = booleans[2];
		switchA(_isTurnA);

		if (!_extraBall && _stepForward){
			if (isA)
				transform.Translate( 1.25f, 0, 0);
			else
				transform.Translate(-1.25f, 0, 0);
		} else if(_extraBall && _stepForward){
			if (isA){
				transform.position = arrowStartPosition + new Vector3(6.25f, 0f,0f);
			} else {
				transform.position = arrowStartPosition + new Vector3(-6.25f, 0f,0f);
			}
		}
		/*
		extraBall = _extraBall;
		Invoke("NextArrowPosition", TIME_DELAY); //FIXME: hardcoded: 3f, line 9
		*/
	}

	// deprecated
	/*
	void NextArrowPosition(){
		if (!extraBall){
			if (isA)
				transform.Translate( 1.25f, 0, 0);
			else
				transform.Translate(-1.25f, 0, 0);
		}
	}*/

	public void ResetArrowPosition(){
		transform.position = arrowStartPosition;
	}

	// deprecated
	/*
	void CancelDelayedMethods(){
		CancelInvoke("NextArrowPosition");
		CancelInvoke("IsTurnANoParam");
	}*/
}
