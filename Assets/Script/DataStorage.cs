﻿using UnityEngine;
using System.Collections;

public static class DataStorage {
	public static float MINIMUM_DELAY_BETWEEN_TWO_ATTEMPTS = 1f;
	public static float TIME_SHOWING_FINAL_SCORE = 5f;
	public static bool DEBUG = true;
	public static bool BALL_TRAIL = true;

	// Use this for initialization
	static void Start () {
	
	}

	// Update is called once per frame
	static void Update () {
	
	}
}
