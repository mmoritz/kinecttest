﻿using UnityEngine;
using System.Collections;

public class MouseFootControl : MonoBehaviour {

	Vector3 mouseDownPosition;
	Vector3 footStartPosition;
	Quaternion footStartRotation;
	float averageVelocity;
	int velocityArraySize = 3;
	int velocityIndex;
	float[] velocityArray;
	bool kick;
	float kickStrenght = 1200f;
	//GameObject goleiro;
	//bool computerTurn = false;
	public bool isRight;
	GameObject ankle, foot;
	Texture2D mTexture;

	// Use this for initialization
	void Start () {
		//goleiro = GameObject.FindGameObjectWithTag("Goleiro");
		mouseDownPosition = Vector3.zero;
		footStartPosition = transform.position;
		footStartRotation = transform.rotation;
		averageVelocity = 1f;
		velocityArray = new float[velocityArraySize];
		velocityIndex = 0;
		for (int i=0; i<velocityArray.Length;i++){
			velocityArray[i] = 1f;
		}
		kick = false;

		GameObject[] feetAndankles = GameObject.FindGameObjectsWithTag("Peh");
		foreach(GameObject bone in feetAndankles)
		{
			if(isRight) {
				if(bone.name == "42_Ankle_Right") {
					ankle = bone;
				} else if(bone.name == "43_Foot_Right") {
					foot  = bone;
				}
			} else {
				if(bone.name == "32_Ankle_Left") {
					ankle = bone;
				} else if(bone.name == "33_Foot_Left") {
					foot  = bone;
				}
			}
		}

		SetTexture2D();
	}
	
	// Update is called once per frame
	void Update () {
		AccompanyankleAndFoot();
		//Debug.DrawLine(transform.position, transform.position + transform.up);
		// collider so fica ativo se o goleiro estiver no estado waiting
		/*
		if (goleiro){
			collider.enabled = goleiro.GetComponent<AnimatorController>().IsWaiting() && !computerTurn;
			renderer.enabled = goleiro.GetComponent<AnimatorController>().IsWaiting() && !computerTurn;
		}*/

		if (velocityIndex == velocityArraySize -1)
			velocityIndex = 0;
		else
			velocityIndex++;
		velocityArray[velocityIndex] = rigidbody.velocity.magnitude;
		averageVelocity = 0;
		foreach (float velocity in velocityArray){
			averageVelocity+=velocity;
		}
		averageVelocity/=velocityArraySize;

		//transform.position = new Vector3(transform.position.x, transform.position.y+0.01f*Input.GetAxis("Vertical"),transform.position.z);
		if (kick){
			transform.position = transform.position + new Vector3(0,0,.2f);
		}
		if (Input.GetButton("Fire2") || transform.position.z > -16f){ // FIXME: hardcoded, ball position
			GoBackToStartPosition();
		}
			
	}

	void OnMouseDown(){
		//Debug.Log("mouseDown");
		mouseDownPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
	}

	void OnMouseDrag(){
		//Debug.Log("mouseDragged");
		Vector3 newPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

		// mudança em y -> movimento em z 
		//float moveZ = 0;
		float moveY = 0;
		float moveX = 0;
		moveX = (newPosition.x - mouseDownPosition.x);
		moveY = (newPosition.y - mouseDownPosition.y);
		//moveY = 19.5f + transform.position.z;
		//moveZ = (newPosition.y - mouseDownPosition.y);
		transform.position = new Vector3 (footStartPosition.x + moveX, 
		                                  footStartPosition.y + moveY, 
		                                  footStartPosition.z);
		//Debug.Log("new Z = " + transform.position.z);
	}

	void OnMouseUp(){
		if(Input.GetButtonUp("Fire1"))
			kick = true;
	}
	
	void OnCollisionEnter(Collision collision){
		//Debug.Log("average velocity " + averageVelocity);
		if(collision.gameObject.tag.Equals("Bola")){

			// Send signal to ball to change state
			collision.gameObject.SendMessage("GotKicked", SendMessageOptions.DontRequireReceiver);

			// deve ser a direçao entre o foot.transform.position - ankle.transform.position
			Vector3 footVector = transform.up;
			Vector3 normalVector = -collision.contacts[0].normal;
			Vector3 footVectorNormal = Vector3.Project(footVector, normalVector);
			//Debug.Log(footVectorNormal.magnitude);
			collision.gameObject.rigidbody.AddForce((0.8f + 0.5f*footVectorNormal.magnitude) * kickStrenght * 
			                                        normalVector, 
			                                        ForceMode.Impulse);
			collision.gameObject.rigidbody.AddTorque(kickStrenght * 2000000000000f * Vector3.Cross(footVector, normalVector), ForceMode.Impulse);

			bool debugVectors = false;
			if (debugVectors){
				//Debug.Log(kickStrenght * Vector3.Cross(footVector, normalVector));
				Debug.DrawLine(collision.transform.position, collision.transform.position+footVector, Color.red);
				Debug.DrawLine(collision.transform.position, collision.transform.position+normalVector, Color.blue);
				Debug.DrawLine(collision.transform.position, collision.transform.position+Vector3.Cross(footVector, normalVector), Color.yellow);
				Time.timeScale = 0;
			}

			// OLD SCHOOL
			//foreach (ContactPoint contact in collision.contacts){
				//collision.gameObject.rigidbody.AddForce(kickStrenght*-contact.normal, ForceMode.Impulse);
			//}

		}
		GoBackToStartPosition ();
	}

	void GoBackToStartPosition(){
		transform.position = footStartPosition;
		transform.rotation = footStartRotation;
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
		kick = false;
	}

	void SetTexture2D(){
		mTexture = new Texture2D(1,1);
		
		for (int y=0;y<mTexture.height;y++){
			for(int x=0;x<mTexture.width;x++){
				mTexture.SetPixel(x,y,Color.red);
			}
		}
		mTexture.Apply();
	}

	void OnGUI(){

		if (DataStorage.DEBUG){



			// Angle between de global up direction and the direction that probe's nose is pointing
			//string debuggingString1 = GUI.TextArea(new Rect(20, 200, 100, 40), "Força do chute: " , 200);
			//string debuggingString2 = GUI.TextArea(new Rect(20, 240, 40, 40), kickStrenght.ToString(), 200);
			//float hSliderValue = kickStrenght;
			int labelStartPosX, labelStartPosY, labelSizeX, labelSizeY;
			int sliderStartPosX, sliderStartPosY, sliderSizeX, sliderSizeY;
			//int stepX, stepY;
			labelStartPosX = 5;
			labelStartPosY = 15;
			labelSizeX = 100;
			labelSizeY = 20;
			sliderStartPosX = 10;
			sliderStartPosY = 35;
			sliderSizeX = 60;
			sliderSizeY = 30;
			//stepX = 115;
			//stepY = 25;


			GUI.Label(new Rect(labelStartPosX, labelStartPosY, labelSizeX, labelSizeY), 
			          "Força: " + ((int)kickStrenght).ToString());
			//GUI.Label(new Rect(labelStartPosX+stepX, labelSizeY, labelSizeX, labelSizeY), 
			          //kickStrenght.ToString());
			float _oldKickStrenght = kickStrenght;
			kickStrenght = GUI.HorizontalSlider(new Rect(sliderStartPosX, sliderStartPosY, sliderSizeX, sliderSizeY), 
			                                    kickStrenght, 0.0F, 3000.0F);

			if(_oldKickStrenght != kickStrenght){
				foreach(GameObject mouseFootObj in GameObject.FindGameObjectsWithTag("Kinect")){
					if (mouseFootObj.GetComponent<MouseFootControl>()){
						mouseFootObj.GetComponent<MouseFootControl>().SetKickStrenght(kickStrenght);
					}
				}
			}



			//float.TryParse(debuggingString2, out kickStrenght);
			//Debug.Log (debuggingString1);
			//Debug.Log (debuggingString2);
		}
	}

	public void SetKickStrenght(float _kickStrenght){
		kickStrenght = _kickStrenght;
	}

	void ComputerTurn(bool _notPlayer){
		//computerTurn = _notPlayer;
	}

	public void AccompanyankleAndFoot()
	{
		//	Position
		if (ankle && foot){
			Vector3 pos = new Vector3();
			pos = (foot.transform.position + ankle.transform.position)/2.0f;
			gameObject.transform.position = pos;
			
			//	Orientation
			Vector3 ankleToFoot_versor = (foot.transform.position - ankle.transform.position).normalized;
			//gameObject.transform.rotation.SetLookRotation(ankleToFoot_versor);	nao funciona, que nem com position
			Quaternion rotation = new Quaternion();
			rotation.SetFromToRotation(Vector3.up, ankleToFoot_versor);
			gameObject.transform.localRotation = rotation;
		}
	}
}
