﻿using UnityEngine;
using System.Collections;

public class Ball2DController : MonoBehaviour {
	GameObject ball;
	BallControl scriptBallControl;
	bool enablePegou = true;
	float tol = 1f;
	float speedChange = 5f;

	// Use this for initialization
	void Start () {
		ball = GameObject.FindGameObjectWithTag ("Bola") as GameObject;
		scriptBallControl = ball.GetComponent<BallControl>();

		// keep the collider disabled until the ball is in the same plane as the goal keeper 
		collider2D.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		IsInTheGoalKeeperPlane ();
		transform.position = transform.parent.transform.position;

		if (scriptBallControl.CompareWithCurrentState(BallControl.BallState.Reseting)) {
			enablePegou = true;
		}
		if(Input.GetButton("Fire2")) {
			//Time.timeScale = 1f;
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.name.Equals("Goleiro") && enablePegou){
			Vector3 currentVelocity = transform.parent.gameObject.rigidbody.velocity;
			transform.parent.gameObject.rigidbody.velocity = new Vector3 (currentVelocity.x+Random.Range(-speedChange,speedChange), currentVelocity.y+Random.Range(-speedChange, speedChange), -currentVelocity.z/3);
			//Debug.Log("Pegou!");
			enablePegou = false;

			//Time.timeScale = 0;
		}
		//Debug.Log (collision.gameObject.name);
	}

	// so habilita o collider da bola se ela estiver bem perto do plano do goleiro
	// motivo, colliders2D ignoram a coordenada z
	void IsInTheGoalKeeperPlane (){
		if (Mathf.Abs(transform.position.z) < tol) {
			collider2D.enabled = true;
		} 
		else {
			collider2D.enabled = false;
		}
	}
}
