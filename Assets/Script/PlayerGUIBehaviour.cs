﻿using UnityEngine;
using System.Collections;

public class PlayerGUIBehaviour : MonoBehaviour {

	public string [] playersNames;
	public bool isA;
	// {"8 - Messi", "10 - Maradona", "9 - Batistuta", "11 - Tevez", "14 - Mascherano", "7 - Crespo", "12 - Evita Peron"};
	// {"7 - Allejo", "1000 - Pele", "7 - Garrincha", "10 - Zico", "11 - Neymar Jr.", "13 - Quequeilson", "12 - Rita Cadillac"};

	int currentPlayerIndex;

	// Use this for initialization
	void Start () {
		currentPlayerIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ChangeTextToNextPlayer(bool _isA)
	{
		if (isA == _isA){
			currentPlayerIndex++;
			if(currentPlayerIndex == playersNames.Length) {
				currentPlayerIndex = 0;
			}
			guiText.text = playersNames[currentPlayerIndex];
		}
	}

	void ResetToFirstPlayer()
	{
		currentPlayerIndex = 0;
		guiText.text = playersNames[currentPlayerIndex];
	}

	public void AlignToParent()
	{
		/*Debug.Log("Parent = " + transform.parent);
		float posX = transform.parent.transform.position.x;
		Debug.Log ("Parent position X = " + posX);
		gameObject.transform.position = new Vector3(posX, transform.position.y, transform.position.z);
		Debug.Log ("current position X = " + gameObject.transform.position);*/
	}
}
