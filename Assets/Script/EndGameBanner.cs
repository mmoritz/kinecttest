﻿using UnityEngine;
using System.Collections;

public class EndGameBanner : MonoBehaviour {
	bool showPlacar;
	float minYScale = 0f;
	float maxYScale = 1f;
	float scale;
	float displayStartTime;
	float SCORE_DELAY = DataStorage.TIME_SHOWING_FINAL_SCORE;
	float scalingTimeFraction = 0.2f; // the fraction o TIME_DELAY it takes to scale
	
	
	// Use this for initialization
	void Start () {
		showPlacar = false;
		gameObject.transform.localScale = new Vector3(1f,0f,1f);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (showPlacar && scale < 1f){
			scale = Mathf.Clamp ((Time.time - displayStartTime)/ (scalingTimeFraction * SCORE_DELAY), 
			                     minYScale, maxYScale);
		} else if (!showPlacar && scale > 0f){
			scale = Mathf.Clamp (1f - (Time.time - displayStartTime)/ (scalingTimeFraction * SCORE_DELAY), 
			                     minYScale, maxYScale);
		}
		gameObject.transform.localScale = new Vector3(1f,scale,1f);
	}
	
	void DisplayPlacar(bool _playerAWins){
		//Debug.Log("End Banner");
		displayStartTime = Time.time;
		showPlacar = true;
		Invoke("RemoveBanner", SCORE_DELAY);
	}

	void RemoveBanner(){
		displayStartTime = Time.time;
		showPlacar = false;
	}
}
