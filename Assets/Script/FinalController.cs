﻿using UnityEngine;
using System.Collections;

public class FinalController : MonoBehaviour {
	public GameObject fader;
	public GameObject background;
	public GameObject leftTeam;
	public GameObject rightTeam;
	public GameObject versus;
	//bool soundPlayed = false;
	float delay = 3f;

	// Use this for initialization
	void Start () {
		//versus.renderer.enabled = false;
		Invoke ("GoBack", delay);
	}

	void GoBack(){
		fader.SendMessage("SetEndScene", true, SendMessageOptions.DontRequireReceiver);
	}

	// Update is called once per frame
	void FixedUpdate () {
		/*
		// mostra o sprite Versus quando o time da esquerda passa de -10f em x.
		if (rightTeam.transform.position.x < 10f){
			//versus.renderer.enabled = true;
			versus.GetComponent<RendererFader>().SetSceneStarting(true);
			//versus.GetComponent<RendererFader>().SetSceneEnding(true);
		}

		FirstShowUp(leftTeam);
		FirstShowUp(rightTeam);
		*/

		/*
		// testa se a bandeira da esquerda chegou a sua posiçao final
		if (Mathf.Abs(leftTeam.transform.position.x - leftTeam.GetComponent<MoveSprite>().GetEndPositionVar()) 
		    <= Mathf.Abs (leftTeam.GetComponent<MoveSprite>().GetScrollSpeed()))
		{
			rightTeam.GetComponent<MoveSprite>().SetIsMoving(true);
		}*/

		/*
		// testa se as duas bandeiras chegaram a sua posiçao final
		if (Mathf.Abs(leftTeam.transform.position.x - leftTeam.GetComponent<MoveSprite>().GetEndPositionVar()) 
		    <= Mathf.Abs (leftTeam.GetComponent<MoveSprite>().GetScrollSpeed()) &&
		    Mathf.Abs(rightTeam.transform.position.x - rightTeam.GetComponent<MoveSprite>().GetEndPositionVar()) 
		    <= Mathf.Abs (rightTeam.GetComponent<MoveSprite>().GetScrollSpeed()))
		{
			if (Mathf.Abs(background.transform.position.y - background.GetComponent<MoveSprite>().GetEndPositionVar()) 
			    <= background.GetComponent<MoveSprite>().GetScrollSpeed())
			{
				fader.GetComponent<SceneFadeInOut>().SetEndScene(true);
			}
			else {
				if (!soundPlayed){
					audio.Play ();
					soundPlayed = true;
				}
				//background.GetComponent<MoveSprite>().SetIsMoving(true);
			}

		}*/

	}

	/*
	void FirstShowUp(GameObject _gameObject){
		if (_gameObject.GetComponent<MoveSprite>()){
			MoveSprite mSprite = _gameObject.GetComponent<MoveSprite>();
			if (Mathf.Abs(Mathf.Abs (_gameObject.transform.position.x) - 25f) <= 
			    Mathf.Abs (mSprite.GetScrollSpeed()/2f))
			{
				_gameObject.audio.Play ();
			}
		}
	}*/
}
